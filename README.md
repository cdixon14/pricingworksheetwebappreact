## Initial State of Project
This project features an implementation of an Excel Web Add-in. The Add-in allows Pariveda employees to authenticate with our Microsoft Active Directory organization and make a simple call to the `/client` endpoint of The Ocean API. This is based on a sample auth project written by microsoft that [can be found here](https://github.com/OfficeDev/PnP-OfficeAddins/tree/master/Samples/auth/Office-Add-in-Microsoft-Graph-React).

As of its initial commit, the follow procedure is possible while running the app:

* Excel application opens with an extra ribbon icon labeled, "Auth PoC".

* Clicking on that icon will open a task pane that displays a locally running web app.

* A button within the pane opens a dialog wherein the user can enter their Pariveda credentials and MFA code to authenticate with our organization.

* After successful authentication, new options appear in the task pane.

* One new button fetches client data from The Ocean and populates the Excel document.

## Goals for Project (7/24/20)
The next functional task to be tackled involves moving any of the API calls that aren't the initial authentication (at the moment, this only includes the /clients endpoint call) out of the scope of the web app and into the scope of VBA code within the Excel document. The reason for this change is to keep any API-calling controls (e.g. buttons) within the literal worksheet UI, as opposed to requiring the user to interact with the web app's task pane to send/get data. The latter would require a degree of training that can be avoided by placing controls directly in the worksheet, which can be done in VBA.
To accomplish this, users will complete the authentication process in the task pane once, and the access token that is received will be written to a cell in the worksheet which can later be read by the VBA code for inclusion in further API calls.

### Update 7/28/20
Changes have been made to this project to correct the formatting for its use with a specific Excel document, found on the Microsoft Teams Files section called ExcelPoCV2.xslx. That sheet contains VBA code that can successfully reach the /clients endpoint as long as this add-in provides the auth token. For this reason, the Get Clients and Clear Sheet buttons in this project have been disabled (although the backing methods are still there for future reference). The retrieval of client data can be started by pressing a button directly on the Excel worksheet, and is now a job of the VBA, not this app.

## Prerequisites

To run this code sample, the following are required.

* [Node and npm](https://nodejs.org/en/), version 10.15.3 or later (npm version 6.2.0 or later). The sample was not tested on earlier versions, but may work with them.

* TypeScript version 3.1.6 or later. The sample was not tested on earlier versions, but may work with them.

* Office on Windows/Mac, version 16.0.6769.2001 or higher.

* A code editor.

## Build and run the solution

Open two command prompts or terminals. One will run the server for the web app, and the other will sideload the project with Excel. Start the server with `npm start`. Once the console suggests the server has been built and is running, use `npm run sideload` in the second terminal to open Excel with the project sideloaded.

## Additional resources

* [Microsoft Graph documentation](https://docs.microsoft.com/graph/)
* [Office Add-ins documentation](https://docs.microsoft.com/office/dev/add-ins/overview/office-add-ins)

## Base Microsoft Project Copyright Information
Copyright (c) 2019 Microsoft Corporation. All rights reserved.

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information, see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.

<img src="https://telemetry.sharepointpnp.com/officedev/samples/readme-template" />
