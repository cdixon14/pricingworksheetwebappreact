/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

import * as msal from 'msal';

(() => {
  // The initialize function must be run each time a new page is loaded
  Office.initialize = () => {

    // This config connects to The Ocean API
    // const config: msal.Configuration = {
    //   auth: {
    //     clientId: 'df1c6122-1c4d-4a39-806f-796fe08c9e12',
    //     authority: 'https://login.microsoftonline.com/be32aae3-7a3a-4fdb-b71a-161014dd062d/',
    //     redirectUri: 'https://localhost:3000/login/login.html'
    //   },
    //   cache: {
    //     cacheLocation: 'localStorage', // needed to avoid "login required" error
    //     storeAuthStateInCookie: true   // recommended to avoid certain IE/Edge issues
    //   }
    // };

    // This config connects to the Pariveda Data API
    const config: msal.Configuration = {
      auth: {
        clientId: '1fe030f0-9390-4403-9b6f-42674f8c09c5',
        authority: 'https://login.microsoftonline.com/be32aae3-7a3a-4fdb-b71a-161014dd062d/',
        redirectUri: 'https://localhost:3000/login/login.html',
      },
      cache: {
        cacheLocation: 'localStorage', // needed to avoid "login required" error
        storeAuthStateInCookie: true   // recommended to avoid certain IE/Edge issues
      }
    };

    const userAgentApp: msal.UserAgentApplication = new msal.UserAgentApplication(config);

    const authCallback = (error: msal.AuthError, response: msal.AuthResponse) => {
      if (!error) {
        if (response.tokenType === 'id_token') {
         localStorage.setItem("loggedIn", "yes");
        }
        else {
          // The tokenType is access_token, so send success message and token.
          Office.context.ui.messageParent( JSON.stringify({ status: 'success', result : response.accessToken }) );
        }
      }
      else {
        const errorData: string = `errorMessage: ${error.errorCode}
                                   message: ${error.errorMessage}
                                   errorCode: ${error.stack}`;
        Office.context.ui.messageParent( JSON.stringify({ status: 'failure', result: errorData }));
      }
    };

    userAgentApp.handleRedirectCallback(authCallback);

    const request: msal.AuthenticationParameters = {
      // scopes: ['https://Pariveda.onmicrosoft.com/Pariveda.TheOcean.API/.default'],
      scopes: ['https://Pariveda.onmicrosoft.com/Pariveda.Data.Api/user_impersonation'],
    };

    if (localStorage.getItem("loggedIn") === "yes") {
      userAgentApp.acquireTokenRedirect(request);
    }
    else {
        // This will login the user and then the (response.tokenType === "id_token")
        // path in authCallback below will run, which sets localStorage.loggedIn to "yes"
        // and then the dialog is redirected back to this script, so the 
        // acquireTokenRedirect above runs.
        userAgentApp.loginRedirect(request);
    }
  };
})();
