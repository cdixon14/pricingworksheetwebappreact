import * as React from 'react';
import { Button, ButtonType } from 'office-ui-fabric-react';

export interface GetDataPageBodyProps {
    logout: () => {};
    getCosts: () => {};
    readData: () => {};
}

export default class GetDataPageBody extends React.Component<GetDataPageBodyProps> {
    render() {
        // const { logout, getCosts, readData } = this.props;
        const { logout, getCosts } = this.props;

        return (
            <div className='ms-welcome__main'>
                <h2 className='ms-font-xl ms-fontWeight-semilight ms-fontColor-neutralPrimary ms-u-slideUpIn20'>Use the buttons below to reach Pariveda Data API endpoints.</h2>
                <Button className='ms-welcome__action' buttonType={ButtonType.hero} iconProps={{ iconName: 'ChevronRight' }} onClick={getCosts}>Get Costs</Button>
                {/* <Button className='ms-welcome__action' buttonType={ButtonType.hero} iconProps={{ iconName: 'ChevronRight' }} onClick={readData}>Read Data</Button> */}
                <Button className='ms-welcome__action' buttonType={ButtonType.hero} iconProps={{ iconName: 'ChevronRight' }} onClick={logout}>Sign Out</Button>
            </div>
        );
    }
}
