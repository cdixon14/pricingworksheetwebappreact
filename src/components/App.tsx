import * as React from 'react';
import { Spinner, SpinnerType } from 'office-ui-fabric-react';
import Header from './Header';
import { HeroListItem } from './HeroList';
import Progress from './Progress';
import StartPageBody from './StartPageBody';
import GetDataPageBody from './GetDataPageBody';
import SuccessPageBody from './SuccessPageBody';
import OfficeAddinMessageBar from './OfficeAddinMessageBar';
import { httpGet } from '../../utilities/api-helper';
// import { httpGetFile } from '../../utilities/api-helper';
import { signInO365, logoutFromO365, writeCostsToSheet, displayRangeData } from '../../utilities/office-apis-helpers';
// import { signInO365, logoutFromO365, displayRangeData } from '../../utilities/office-apis-helpers';


export interface AppProps {
    title: string;
    isOfficeInitialized: boolean;
}

export interface AppState {
    authStatus?: string;
    fileFetch?: string;
    headerMessage?: string;
    errorMessage?: string;
}

export default class App extends React.Component<AppProps, AppState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            authStatus: 'notLoggedIn',
            fileFetch: 'notFetched',
            headerMessage: 'Welcome',
            errorMessage: ''
        };

        // Bind the methods that we want to pass to, and call in, a separate
        // module to this component. And rename setState to boundSetState
        // so code that passes boundSetState is more self-documenting.
        this.boundSetState = this.setState.bind(this);
        this.setToken = this.setToken.bind(this);
        this.displayError = this.displayError.bind(this);
        this.loginOnClick = this.loginOnClick.bind(this);
    }

    /*
        Properties
    */

    // The access token is not part of state because React is all about the
    // UI and the token is not used to affect the UI in any way.
    accessToken: string;

    listItems: HeroListItem[] = [
        {
            icon: 'PlugConnected',
            primaryText: 'Authenticates with the Microsoft Pariveda organization.'
        },
        {
            icon: 'ExcelDocument',
            primaryText: 'Calls an endpoint belonging to the Pariveda Data API that returns cost data by cohort.'
        },
        {
            icon: 'AddNotes',
            primaryText: 'Prints the received cost values to the open spreadsheet.'
        }
    ];

    /*
        Methods
    */

    boundSetState: () => {};

    setToken = async (accessToken: string) => {
        this.accessToken = accessToken;

        // Also send accessToken to Excel, to be used by VBA, reference, etc.
        return Excel.run((context: Excel.RequestContext) => {
            const sheet = context.workbook.worksheets.getActiveWorksheet();
            sheet.getRange("F17").values = [["Token:"]];
            sheet.getRange("G17").values = [[accessToken]];
            sheet.getRange("H17").values = [[" "]];
            
            return context.sync();
        })
        .catch( (error) => {
            this.displayError(error.toString());
        });
    }

    displayError = (error: string) => {
        this.setState({ errorMessage: error });
    }

    // Runs when the user clicks the X to close the message bar where
    // the error appears.
    errorDismissed = () => {
        this.setState({ errorMessage: '' });

        // If the error occurred during a "in process" phase (logging in or getting files),
        // the action didn't complete, so return the UI to the preceding state/view.
        this.setState((prevState) => {
            if (prevState.authStatus === 'loginInProcess') {
                return {authStatus: 'notLoggedIn'};
            }
            else if (prevState.fileFetch === 'fetchInProcess') {
                return {fileFetch: 'notFetched'};
            }
            return null;
        });
    }

    loginOnClick = async () => {
        await signInO365(this.boundSetState, this.setToken, this.displayError);
    }

    logoutOnClick = async () => {
        await logoutFromO365(this.boundSetState, this.displayError);
    }

    getCostsOnClick = async () => {
        this.setState({ fileFetch: 'fetchInProcess' });

        httpGet("https://parivedadataapi.azurewebsites.net/api/PricingWorksheet/costs", this.accessToken)
            .then( async (response) => {
                await writeCostsToSheet(response, this.displayError);
                this.setState({ fileFetch: 'fetched', headerMessage: 'Success' });
            })
            .catch( (requestError) => {
                this.setState({ errorMessage: requestError });
            });
        // this.setState({ fileFetch: 'fetchInProcess' });

        // httpGetFile()
        //     .then( async (response) => {
        //         this.displayError("Success: " + response.data);
        //         this.setState({ fileFetch: 'fetched', headerMessage: 'Success' });
        //     })
        //     .catch( (requestError) => {
        //         this.setState({ errorMessage: requestError });
        //     });

    }

    readDataOnClick = async () => {
        await displayRangeData(this.displayError);
    }

    render() {
        const { title, isOfficeInitialized } = this.props;

        if (!isOfficeInitialized) {
            return (
                <Progress
                    title={title}
                    logo='assets/Onedrive_Charts_icon_80x80px.png'
                    message='Please sideload your add-in to see app body.'
                />
            );
        }

        // Set the body of the page based on where the user is in the workflow.
        let body;

        if (this.state.authStatus === 'notLoggedIn') {
            body = ( <StartPageBody login={this.loginOnClick} listItems={this.listItems}/> );
        }
        else if (this.state.authStatus === 'loginInProcess') {
            body = ( <Spinner className='spinner' type={SpinnerType.large} label='Please sign-in on the pop-up window.' /> );
        }
        else {
            if (this.state.fileFetch === 'notFetched') {
                body = ( <GetDataPageBody readData={this.readDataOnClick} getCosts={this.getCostsOnClick} logout={this.logoutOnClick}/> );
            }
            else if (this.state.fileFetch === 'fetchInProcess') {
                body = ( <Spinner className='spinner' type={SpinnerType.large} label='We are getting the data for you.' /> );
            }
            else {
                body = ( <SuccessPageBody readData={this.readDataOnClick} getCosts={this.getCostsOnClick} logout={this.logoutOnClick}/> );
            }
        }

        return (
            <div>
                { this.state.errorMessage ?
                  (<OfficeAddinMessageBar onDismiss={this.errorDismissed} message={this.state.errorMessage + ' '} />)
                : null }

                <div className='ms-welcome'>
                    <Header logo='assets/Onedrive_Charts_icon_80x80px.png' title={this.props.title} message={this.state.headerMessage} />
                    {body}
                </div>
            </div>
        );
    }
}
