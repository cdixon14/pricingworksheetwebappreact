import { AppState } from '../src/components/App';
import { AxiosResponse } from 'axios';

/*
    Interacting with the Office document
*/
export const writeClientsToSheet = async (result: AxiosResponse, displayError: (x: string) => void) => {
    return Excel.run( (context: Excel.RequestContext) => {
        const sheet = context.workbook.worksheets.getActiveWorksheet();

        // Set Headings
        const header = sheet.getRange('A1:C1');
        header.values = [['ID', 'Name', 'Industry']];
        header.format.font.bold = true;
        header.format.font.size = 16;

        // Print all rows of client data
        const allClients = result.data["clients"];
        for (var i = 0; i < result.data["total"]; i++) {
            sheet.getCell(i + 1, 0).values = [[allClients[i]['id']]];
            sheet.getCell(i + 1, 1).values = [[allClients[i]['name']]];
            sheet.getCell(i + 1, 2).values = [[allClients[i]['industry']]];
        }
        
        return context.sync();
    })
    .catch( (error) => {
        displayError(error.toString());
    });
}

export const writeCostsToSheet = async (result: AxiosResponse, displayError: (x: string) => void) => {
    return Excel.run( (context: Excel.RequestContext) => {
        const sheet = context.workbook.worksheets.getActiveWorksheet();

        const header = sheet.getRange('A1:C1');
        header.values = [['Level', 'Office', 'Cost']]
        header.format.font.bold = true;
        header.format.font.size = 16;

        for (var i = 0; i < result.data.length; i++) {
            sheet.getCell(i + 1, 0).values = [[result.data[i]['Level']]];
            sheet.getCell(i + 1, 1).values = [[result.data[i]['Office']]];
            sheet.getCell(i + 1, 2).values = [[result.data[i]['Cost']]];
        }

        return context.sync();
    })
    .catch( (error) => {
        displayError(error.message);
    })
}

export const displayRangeData = async (displayError: (x: string) => void) => {
    return Excel.run( async (context: Excel.RequestContext) => {
        const sheet = context.workbook.worksheets.getActiveWorksheet();

        // Make sure there's some data in C2:C10
        // This will display it in an a red error banner
        var range = sheet.getRange("C2:C10");
        range.load("values");
        await context.sync();

        displayError('values: ' + JSON.stringify(range.values));
    })
    .catch( (error) => {
        displayError(error.toString());
    });
}

export const initWorkbook = async () => {
    // Create additional hidden sheet
    Excel.run( async (context) => {
        const sheets = context.workbook.worksheets;
        sheets.load("items/name");
        await context.sync();

        // If HiddenSheet is already present, just clear it and return
        var lastSheet = sheets.items[sheets.items.length - 1];
        console.log("Name of last sheet: " + lastSheet.name.toString());
        if (lastSheet.name === "HiddenSheet") {
            lastSheet.getRange("A1:E5").delete(Excel.DeleteShiftDirection.left);
            console.log("HiddenSheet had already been created, its old contents have been cleared.");
        }
        else {
            var newSheet = sheets.add("HiddenSheet");
            newSheet.visibility = "Hidden";
            console.log("Hidden sheet successfully initialized.")
        }

        await context.sync();
    });

    // Register listener for cell on that hidden sheet
    await registerRangeListener();
}

const registerRangeListener = async () => {
    Excel.run(async function (context) {
        
        // var table = context.workbook.tables.add("HiddenSheet!A1:A2", true);
        context.workbook.names.add("flagCell", "HiddenSheet!A1");
        
        try {
            await context.sync();

            //Create a new table binding for flagTable
            Office.context.document.bindings.addFromNamedItemAsync("flagCell", Office.BindingType.Text, { id: "flagCellBinding" }, function (asyncResult) {
                if (asyncResult.status == Office.AsyncResultStatus.Failed) {
                    console.log("Adding cell binding failed with error: " + asyncResult.error.message);
                }
                else {
                    // If successful, add the event handler to the table binding.
                    Office.select("bindings#flagTableBinding").addHandlerAsync(Office.EventType.BindingDataChanged, onFlagCellChanged);
                    console.log("Handler registered successfully.");
                }
            });
        }
        catch (error) {
            console.log(JSON.stringify(error));
        }
    });
}

async function onFlagCellChanged(eventArgs) {
    Excel.run(function (context) {
        // Take and log data in 'Sheet1!D2:D10'
        const sheet = context.workbook.worksheets.getItem('Sheet1');
        var range = sheet.getRange('F2:F10');
        range.load('values');
        return context.sync()
            .then(() => {
                console.log("EventArgs: " + eventArgs.toString() + "\nRead values: " + JSON.stringify(range.values));
            })
            .catch( (e) => {
                console.log("Error occurred while reading values: " + e.toString());
            });
    });
}


/*
    Managing the dialogs.
*/
let loginDialog: Office.Dialog;
const dialogLoginUrl: string = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/login/login.html';

export const signInO365 = async (setState: (x: AppState) => void,
                                 setToken: (x: string) => void,
                                 displayError: (x: string) => void) => {

    setState({ authStatus: 'loginInProcess' });

    Office.context.ui.displayDialogAsync(
        dialogLoginUrl,
        {height: 40, width: 30},
        (result) => {
            if (result.status === Office.AsyncResultStatus.Failed) {
                displayError(`${result.error.code} ${result.error.message}`);
            }
            else {
                loginDialog = result.value;
                loginDialog.addEventHandler(Office.EventType.DialogMessageReceived, processLoginMessage);
                loginDialog.addEventHandler(Office.EventType.DialogEventReceived, processLoginDialogEvent);
            }
        }
    );

    const processLoginMessage = async (arg: {message: string, type: string}) => {
        
        let messageFromDialog = JSON.parse(arg.message);
        if (messageFromDialog.status === 'success') { 

            // We now have a valid access token.
            loginDialog.close();
            setToken(messageFromDialog.result);
            setState( { authStatus: 'loggedIn',
                        headerMessage: 'Call API' });
        }
        else {
            // Something went wrong with authentication or the authorization of the web application.
            loginDialog.close();
            displayError(messageFromDialog.result);
        }
    };

    const processLoginDialogEvent = (arg) => {
        processDialogEvent(arg, setState, displayError);
    };
};

let logoutDialog: Office.Dialog;
const dialogLogoutUrl: string = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/logout/logout.html';

export const logoutFromO365 = async (setState: (x: AppState) => void,
                                     displayError: (x: string) => void) => {

    Office.context.ui.displayDialogAsync(dialogLogoutUrl,
        {height: 40, width: 30},
        (result) => {
            if (result.status === Office.AsyncResultStatus.Failed) {
                displayError(`${result.error.code} ${result.error.message}`);
            }
            else {
                logoutDialog = result.value;
                logoutDialog.addEventHandler(Office.EventType.DialogMessageReceived, processLogoutMessage);
                logoutDialog.addEventHandler(Office.EventType.DialogEventReceived, processLogoutDialogEvent);
            }
        }
    );

    const processLogoutMessage = () => {
        logoutDialog.close();
        setState({ authStatus: 'notLoggedIn',
                   headerMessage: 'Welcome' });
    };

    const processLogoutDialogEvent = (arg) => {
        processDialogEvent(arg, setState, displayError);
    };
};

const processDialogEvent = (arg: {error: number, type: string},
                            setState: (x: AppState) => void,
                            displayError: (x: string) => void) => {

    switch (arg.error) {
        case 12002:
            displayError('The dialog box has been directed to a page that it cannot find or load, or the URL syntax is invalid.');
            break;
        case 12003:
            displayError('The dialog box has been directed to a URL with the HTTP protocol. HTTPS is required.');
            break;
        case 12006:
            // 12006 means that the user closed the dialog instead of waiting for it to close.
            // It is not known if the user completed the login or logout, so assume the user is
            // logged out and revert to the app's starting state. It does no harm for a user to
            // press the login button again even if the user is logged in.
            setState({ authStatus: 'notLoggedIn',
                       headerMessage: 'Welcome' });
            break;
        default:
            displayError('Unknown error in dialog box.');
            break;
    }
};

