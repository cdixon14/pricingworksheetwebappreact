import axios from 'axios';

export const httpGet = async (url: string, accessToken: string, params: string[] = null) => {
  const response = await axios({
    method: 'get',
    url: url,
    params: params,
    headers: { 'Authorization': `Bearer ${accessToken}`},
  });

  return response;
}

export const httpGetFile = async () => {
  return await axios({
    method: 'get',
    url: 'https://pariveda.sharepoint.com/sites/sales/_cts/Pricing%20Worksheet(-540536743)/Pricing%20Worksheet%20Template_v3.0.1.xlsm',
    responseType: 'blob',
    headers: { 'Accept': 'application/vnd.ms-excel'},
  }).then((response) => {
    // const url = window.URL.createObjectURL(new Blob([response.data]));
    // const link = document.createElement('a');
    // link.href = url;
    // link.setAttribute('download', 'file.pdf');
    // document.body.appendChild(link);
    // link.click();
    return response;
  });
}

// axios({
//   url: 'http://localhost:5000/static/example.pdf',
//   method: 'GET',
//   responseType: 'blob', // important
// }).then((response) => {
//   const url = window.URL.createObjectURL(new Blob([response.data]));
//   const link = document.createElement('a');
//   link.href = url;
//   link.setAttribute('download', 'file.pdf');
//   document.body.appendChild(link);
//   link.click();
// });
